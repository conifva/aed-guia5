#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include "Arbol.h"
#include "Grafo.h"

using namespace std;


void menu(Arbol avl){
	
	int dato;
	int datoNuevo;
	int opcion;
	int contador = 0; //Ordena el arbol visualmente
	bool altura = false; //Indica si la altura del arbol ha crecido

	Nodo *arbol = NULL; //Paso 1: crea arbol vacio

	do {
		/* Nuevo estilo de menu */
		cout << "\t  .:MENU:." << endl;
		cout << " 1. Insertar un nuevo nodo" << endl;
		cout << " 2. Mostrar arbol horizontal" << endl;
		cout << " 3. Buscar un nodo" << endl;
		cout << " 4. Recorrer arbol" << endl;
		cout << " 5. Eliminar un nodo" << endl;
		cout << " 6. Modificar un nodo" << endl;
		cout << " 7. Generar imagen" << endl;
		cout << " 8. Salir" << endl;
		cout << "\n > Ingrese su opción: ";
		cin >> opcion;

		switch(opcion){
			case 1:
				cout << "\n\t :::INSERTAR:::" << endl;
				if (arbol == NULL) {
					cout << "\n ESTADO: <Arbol vacio>" << endl;
					cout << " > Ingrese la raíz: ";
					cin >> dato;
					/* Insertar un nuevo nodo, raiz */
					avl.insertarNodo(arbol, dato, altura);
				} else {
					cout << "\n > Ingrese un número: ";
					cin >> dato;
					/* Insertar un nuevo nodo */
					avl.insertarNodo(arbol, dato, altura);
				}
				cout << "\n";
				break;

			case 2:
				cout << "\n\t :::ARBOL BALANCEADO HORIZONTAL:::" << endl;
				if (arbol == NULL) {
					cout << "\n ESTADO: <Arbol vacio>" << endl;
				} else {
					avl.mostrarArbol(arbol, contador);
				}
				cout << "\n";
				break;

			case 3:
				cout << "\n\t :::BUSQUEDA:::" << endl;
				cout << " > Ingrese el número que busca: ";
				cin >> dato;
				if (avl.busqueda(arbol, dato) == true) {
					/* Coincidencia */
					cout << "\n Elemento <" << dato << "> a sido encontrado en el arbol" << endl;
				} else {
					if (arbol == NULL) {
						cout << "\n ESTADO: <Arbol vacio>";
					}
					cout << "\n Elemento <" << dato << "> no encontrado" << endl;
				}
				cout << "\n";
				break;

			case 4:
				cout << "\n\t :::RECORRIDO:::" << endl;
				if (arbol == NULL) {
					cout << " ESTADO: <Arbol vacio>" << endl;
				} else {
					cout << " <PreOrden>:" << endl; //raiz-izq-der
					avl.preOrden(arbol);
					cout << "\n <PostOrden>:" << endl; //izq-der-raiz
					avl.postOrden(arbol);
					cout << "\n <InOrden>:" << endl; //izq-raiz-der
					avl.inOrden(arbol);
				}
				cout << "\n\n";
				break;

			case 5:
				cout << "\n\t :::ELIMINAR:::" << endl;
				cout << " > Ingrese el número a eliminar: ";
				cin >> dato;
				if (avl.busqueda(arbol, dato) == true) {
					/* Coincidencia */
					cout << "\n Elemento <" << dato << "> localizado" << endl;
					avl.eliminarNodo(arbol, dato, altura);
					cout << " Elemento eliminado con exito" << endl;
				} else {
					if (arbol == NULL) {
						cout << "\n ESTADO: <Arbol vacio>";
					}
					avl.eliminarNodo(arbol, dato, altura);
					//cout << "\n Elemento <" << dato << "> no existe" << endl;
				}
				cout << "\n";
				break;

			case 6:
				cout << "\n\t :::MODIFICAR:::" << endl;
				cout << " > Ingrese el número a modificar: ";
				cin >> dato;
				if (avl.busqueda(arbol, dato) == true) {
					/* Coincidencia */
					cout << "\n Elemento <" << dato << "> localizado" << endl;
					/* Eliminar nodo y sus hijos (si es que tenia) */
					avl.eliminarNodo(arbol, dato, altura);
					cout << " > Ingrese el nuevo número: ";
					cin >> datoNuevo;
					/* Insertar nuevo nodo */
					avl.insertarNodo(arbol, datoNuevo, altura);
					cout << " Elemento modificado con exito" << endl;
				} else {
					if (arbol == NULL) {
						cout << "\n ESTADO: <Arbol vacio>";
					}
					cout << "\n Elemento <" << dato << "> no existe" << endl;
					cout << " Imposible de modificar el elemento" << endl;
				}
				cout << "\n";
				break;

			case 7:
				cout << "\n\t :::GENERAR IMAGEN:::" << endl;
				if (arbol == NULL) {
					cout << "\n ESTADO: <Arbol vacio>" << endl;
					cout << " Gráfico no generado" << endl;
				} else {
					avl.graficar(arbol);
				}
				cout << "\n";
				break;

			case 8:
				exit(1);
				break;

			default:
				cout << "Opcion no valida" << endl;
				cout << "\n";
				menu(avl);
				break;
		}
	} while (opcion != 8);
}

int main(){
	Arbol avl = Arbol();
	menu(avl);

	return 0;
}