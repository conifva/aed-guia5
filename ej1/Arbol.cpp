#include <iostream>
#include <fstream>
#include "Arbol.h"
#include "Grafo.h"

Arbol::Arbol(){}

/* fx para crear un nuevo nodo */
Nodo* Arbol::crearNodo(int n) {
	Nodo *nuevo_nodo; //nuevo_nodo es un nuevo nodo

	nuevo_nodo = new Nodo(); //Reserva memoria
	nuevo_nodo->dato = n; //Asigna valor nodo
	nuevo_nodo->fe = 0; //Factor de equilibrio
	nuevo_nodo->izq = NULL;
	nuevo_nodo->der = NULL;

	return nuevo_nodo;
}

/* fx para insertar elem en el arbol */
void Arbol::insertarNodo(Nodo *&nodo, int n, bool &altura){

	if (nodo != NULL) {
		/* Obtener el valor de la raiz*/
		int valorRaiz = nodo->dato;

		/* Si el elemento es menor a la raiz, insertar en rama izq
		 * e indicar si la altura aumenta o dismunuye */
		if (n < valorRaiz) {
			insertarNodo(nodo->izq, n, altura);

			if (altura == true)	{ //La altura disminuyó
				/* Analisis al factor de equilibrio para ver si esta balancea-
				 * do el árbol */
				switch (nodo->fe) {
					/* Factor de equilibrio ayuda a determinar q metodo de
					 * rotacion utilizar */
					case 1:
						nodo->fe = 0;
						altura = false;
						break;

					case 0:
						nodo->fe = -1;
						break;

					case -1:
						nodo1 = nodo->izq;
						/* Reestructurar arbol */

						if (nodo1->fe <= 0) {
							/* Rotacion Izq-Izq */
							nodo->izq = nodo1->der;
							nodo1->der = nodo;
							nodo->fe = 0;
							nodo = nodo1;
						}
						else {
							/* Rotacion Izq-Der */
							nodo2 = nodo1->der;
							nodo->izq = nodo2->der;
							nodo2->der = nodo;
							nodo1->der = nodo2->izq;
							nodo2->izq = nodo1;

							/* si el factor es -1 */
							if (nodo2->fe == -1) {
								nodo->fe = 1;
							} else {
								nodo->fe = 0;
							}

							/* si el factor es 1 */
							if (nodo2->fe == 1){
								nodo1->fe = -1;
							} else {
								nodo1->fe = 0;
							}
							nodo = nodo2;
						}

						nodo->fe = 0;
						altura = false;
						break;
				}
			}
		}
		/* Otros casos pueden ser; que se tenga que añadir al árbol en rama
		 * derecha o no añadir (elemento repetido) */
		else {
			/* Si el elemento es mayor a la raiz, insertar en rama derecha
			 * e indicar su altura */
			if (n > valorRaiz) {
				insertarNodo(nodo->der, n, altura);

				if (altura == true) {
					/* Analisis al factor de equilibrio (balance) para determinar que
					 * metodo de rotacion utilizar */
					switch(nodo->fe){
						case -1:
							nodo->fe = 0;
							altura = false;
							break;

						case 0:
							nodo->fe = 1;
							break;

						case 1:
							nodo1 = nodo->der;
							/* Reestructurar arbol */

							if (nodo1->fe >= 0) {
								/* Rotacion Der-Der para el proceso de balan-
								 * ceo*/
								nodo->der = nodo1->izq;
								nodo1->izq = nodo;
								nodo->fe = 0;
								nodo = nodo1;
							}
							else {
								/* Rotacion Der-Izq para el proceso de balan-
								 * ceo*/
								nodo2 = nodo1->izq;
								nodo->der = nodo2->izq;
								nodo2->izq = nodo;
								nodo1->izq = nodo2->der;
								nodo2->der = nodo1;

								/* si el factor es 1 */
								if (nodo2->fe == 1) {
									nodo->fe = -1;
								} else {
									nodo->fe = 0;
								}

								/* si el factor es -1 */
								if (nodo2->fe == -1) {
									nodo1->fe = 1;
								} else {
									nodo1->fe = 0;
								}
								nodo = nodo2;
							}
							nodo->fe = 0;
							altura = false;
							break;
					}//fin switch
				}//fin altura true
			}
			/* De lo contrario signifca que el valor que se quiere insertar ya
			 * existe. Como no se permite la duplicidad de este dato no se 
			 * realiza ningun cambio */
			else {
				/* Elemento repetido */
				cout << "El valor " << n << " ya existe en el árbol" << endl;
			}
		}//fin else x der
	}
	/* Si en nodo recibido fuera nulo entonces el nuevo nodo se puede insertar
	 * en esa posición y se terminan las llamadas recursivas a este método */
	else {
		/* CREA UN NUEVO NODO, se le pasa el elemento e indica su padre */
		Nodo *nuevo_nodo = crearNodo(n);
		altura = true; //Hay un cambio en la altura (aumenta)
		nodo = nuevo_nodo; //Almacena dato en la raiz
		cout << " Elemento insertado con exito" << endl;
	}
}

/* fx para mostrar arbol completo de forma horizontal */
void Arbol::mostrarArbol(Nodo *arbol, int cont){ 
	/* Es de tipo puntero porq no voy a cambiar ningun valor */
	/* Contador no cuenta la cant de nodos, es solo para separar (ordenar) la
	 * impresion entre nodos */

	if (arbol == NULL) {
		/* Arbol vacio */
		return;
	} else {
		/* Empieza por el lado derecho */
		mostrarArbol (arbol->der, cont+1 );
		for (int i = 0; i < cont; ++i) {
			cout << "   "; //3 espacios pensando en nodos de 3 digitos
		}
		cout << arbol->dato << endl;
		/* Continuar con el lado izquierdo */
		mostrarArbol(arbol->izq, cont+1 );
	}
}

/* fx para buscar un elemento en el arbol */
bool Arbol::busqueda(Nodo *arbol, int n){
	if (arbol == NULL) {
		/* Arbol vacio */
		return false;
	} else if (arbol->dato == n) {
		/* Si el nodo actual es igual al elemento */
		return true;
	} else if (n < arbol->dato) {
		/* Si la busqueda es menor al nodo, analiza rama izq */
		return busqueda(arbol->izq, n);
	} else {
		/* Si la busqueda es mayor al nodo, analiza rama der */
		return busqueda(arbol->der, n);
	}
}

/* fx para recorrido preOrden */
void Arbol::preOrden(Nodo *arbol){
	if (arbol == NULL) {
		/* Arbol vacio */
		return;
	} else {
		/* Primero la raiz, luego el subarbol izq,
		 * y despues el der */
		cout << arbol->dato << " - ";
		preOrden(arbol->izq);
		preOrden(arbol->der);
	}
}

/* fx para recorrido inOrden */
void Arbol::inOrden(Nodo *arbol){
	if (arbol == NULL) {
		/* Arbol vacio */
		return;
	} else {
		/* Primero el subarbol izq, luego la raiz
		 * y despues el subarbol der */
		inOrden(arbol->izq);
		cout << arbol->dato << " - ";
		inOrden(arbol->der);
	}
}

/* fx para recorrido postOrden */
void Arbol::postOrden(Nodo *arbol){
	if (arbol == NULL) {
		/* Arbol vacio */
		return;
	} else {
		/* Primero el subarbol izq, luego el der
		 * y despues la raiz */
		postOrden(arbol->izq);
		postOrden(arbol->der);
		cout << arbol->dato << " - ";
	}
}

/* fx para eliminar un nodo */
void Arbol::eliminarNodo(Nodo *&nodo, int n, bool &altura){
	// bool indica que la altura del arbol ha disminuido, inicia en false
	/* Usa 2 algoritmos axiliares: reestrucDer y reestrucIzq,
	 * ademas de 3 variables auxiliares de tipo puntero: otro, aux y aux1 */
	if (nodo != NULL) {
		/* Obtener el valor de la raiz*/
		int valorRaiz = nodo->dato;

		/* Si el elemento es menor a la raiz, insertar en izq
		 * e indicar su altura */
		if (n < valorRaiz) {
			eliminarNodo(nodo->izq, n, altura);
			reestrucIzq(nodo, altura);
		}
		else {
			/* Si el elemento es mayor a la raiz, insertar en der
		 	 * e indicar su altura */
			if (n > valorRaiz) {
				eliminarNodo(nodo->der, n, altura);
				reestrucDer(nodo, altura);
			}
			else {
				otro = nodo;
				altura = true; //altura disminuyó

				if (otro->der == NULL) {
					nodo = otro->izq;
				}
				else {
					if (otro->izq == NULL) {
						nodo = otro->der;
					}
					else {
						aux = nodo->izq;
						altura = false;

						while (aux->der != NULL){
							aux1 = aux;
							aux = aux->der;
							altura = true;
						}
						nodo->dato = aux->dato;
						otro = aux;

						if (altura == true) {
							aux1->der = aux->izq;
						}
						else {
							nodo->izq = aux->izq;
						}
						reestrucDer(nodo->izq, altura);
					}
				}
				delete otro; //liberar memoria del nodo
			}
		}
	}
	else {
		cout << "\n Elemento <" << n << "> no existe" << endl;
		cout << " Imposible de eliminar" << endl;
	}
}


/* TODO */
void Arbol::reestrucIzq(Nodo *&nodo, bool &altura){
	//bool pasado por ref, indica q la altura de la rama izq ha disminuido
	/* Se usa cuando la altura de la rama izquierda ha disminuido y el factor
	 * de equilibrio es 1 */
	if (altura == true) { //Altura de la rama izq ha disminuido
		/* Analisis al factor de equilibrio para ver si esta balanceado
		 * el árbol */
		switch (nodo->fe) {
			case -1:
				nodo->fe = 0;
				break;

			case 0:
				nodo->fe = 1;
				altura = false;
				break;

			case 1:
				/* Reestructuración del árbol*/
				nodo1 = nodo->der;
				//factor mayor o igual a 0
				if (nodo1->fe >= 0)	{
					/* Rotación DD para el proceso de balanceo */
					nodo->der = nodo1->izq;
					nodo1->izq = nodo;

					switch (nodo1->fe){
						case 0:
							nodo->fe = 1;
							nodo1->fe = -1;
							altura = false;
							break;

						case -1:
							nodo->fe = 0;
							nodo1->fe = 0;
							break;
					}
					nodo = nodo1;
					//fin rotacion DD
				}
				else {
					/* Rotación DI  para el proceso de balanceo */
					nodo2 = nodo1->izq;
					nodo->der = nodo2->izq;
					nodo2->izq = nodo;
					nodo1->izq = nodo2->der;
					nodo2->der = nodo1;

					if (nodo2->fe == 1) {
						nodo->fe = -1;
					} else {
						nodo->fe = 0;
					}

					if (nodo2->fe == -1) {
						nodo1->fe = 1;
					} else {
						nodo1->fe = 0;
					}

					nodo = nodo2;
					nodo2->fe = 0;
				//fin rotacion DI
				}
				break;
		}
	}
}

/* TODO */
void Arbol::reestrucDer(Nodo *&nodo, bool &altura){
	//bool indica q la altura de la rama der ha disminuido
	/* Se usa cuando la altura de la rama derecha ha disminuido y el factor de
	 * equilibrio es -1 */
	if (altura == true) { //Altura disminuyó
		/* Analisis al factor de equilibrio para ver si esta balanceado el ár-
		 * bol */
		switch (nodo->fe) {
			case 1:
				nodo->fe = 0;
				break;

			case 0:
				nodo->fe = -1;
				altura = false;
				break;

			case -1:
				/* Reestructuración del árbol*/
				nodo1 = nodo->izq;
				//factor menor o igual a 0
				if (nodo1->fe <= 0)	{
					/* Rotación II para el proceso de balanceo */
					nodo->izq = nodo1->der;
					nodo1->der = nodo;

					switch (nodo1->fe){
						case 0:
							nodo->fe = -1;
							nodo1->fe = 1;
							altura = false;
							break;

						case -1:
							nodo->fe = 0;
							nodo1->fe = 0;
							break;
					}
					nodo = nodo1;
				}
				else {
					/* Rotación ID para el proceso de balanceo */
					nodo2 = nodo1->der;
					nodo->izq = nodo2->der;
					nodo2->der = nodo;
					nodo1->der = nodo2->izq;
					nodo2->izq = nodo1;

					if (nodo2->fe == -1) {
						nodo->fe = 1;
					} else {
						nodo->fe = 0;
					}

					if (nodo2->fe == 1) {
						nodo1->fe = -1;
					} else {
						nodo1->fe = 0;
					}

					nodo = nodo2;
					nodo2->fe = 0;
				}
				break;
		}
	}
}

/* fx para generar imagen del arbol */
void Arbol::graficar(Nodo *arbol){
	if (arbol == NULL) {
		/* Arbol vacio */
		return;
	} else {
		cout << " Iniciando creacion del grafico" << endl;
		Grafo *grap = new Grafo(arbol);
	}
}